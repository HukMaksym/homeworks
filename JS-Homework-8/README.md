
## HW8 - searching-dom-tasks

### Відповіді на теоретичні питання:

#### 1. Опишіть своїми словами що таке Document Object Model (DOM)

    DOM - об'єктна модель документу. Це древовидна інтерпретація сайту, у вигляді серії вузлів (об'єктів), які вкладені один в одний. За допомогою DOM ми можемо взаємодіяти з його елементами та вмістом цих елементів - додавати, видаляти та змінювати як вміст елементів так і самі елементи.

#### 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

- `innerText` повертає тільки фактичне текстова значення з кожним елементом у новому рядку у вигляді простого тексту та ігнорує форматування HTML, якщо воно присутнє в елементах

- `innerHTML` повертає HTML-вміст та приміняє форматування HTML

#### 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

За допомогою методів:
- `querySelector(css)` - повертає перший елемент, що задовольняє css-селектору
- `querySelectorAll(css)` - повертає всі елементи, що задовільняють css-селектору
- `getElementByID(id)` - повертє елемент з заданим id
- `getElementsByTagName(tag)` - повертає елементи з заданим тегом
- `getElementsByClassName(className)` - повертає елементи з заданим CSS-класом

Краще використовувати querySelector, бо він більш потужний, ефективний, та універсальний бо може приймати будь-який css-селектор.

### Завдання

1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000

2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
