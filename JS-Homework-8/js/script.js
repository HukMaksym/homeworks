// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((p) => {
    p.style.backgroundColor = '#F00'
})
// ---------------------------------

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
// console.log(optionsList.closest('.container'));
const childNodes = optionsList.childNodes;
// childNodes.forEach((child, idx) => {
//     console.log(childNodes[idx])
// })

if (optionsList.hasChildNodes()) {
    childNodes.forEach((child, idx) => {
        console.log(childNodes[idx])
    })
} else {
    console.log('Element dont have child nodes');
}
// ---------------------------------

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
// TODO:тут мабудь помилка і мається на увазі елемент з id="testParagraph", а не з класом?
// TODO: і також не зовсім зрозуміло навіщо всередині параграфа створювати ще один параграф, можливо буде правильніше просто замінити існуючий параграф новим?

const testPapagpaph = document.querySelector('#testParagraph');
testPapagpaph.innerHTML = '<p>This is a paragraph</p>';
// можливо так буде більш правильно?:
// testPapagpaph.innerHTML = 'This is a paragraph';
// ---------------------------------

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const mainHeader = document.querySelector('.main-header');
const listOfLi = mainHeader.querySelectorAll('li');
console.log(listOfLi);
listOfLi.forEach((el) => {
    el.classList.add('nav-item');
});
// ---------------------------------

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach((el) => {
    el.classList.remove('section-title')
})