document.addEventListener('keydown', (evt) => {
    const btns = document.querySelectorAll('.btn');
    const pressedKey = evt.code;

    for (i = 0; i < btns.length; i++) {
        if (btns[i].dataset.key == pressedKey) {
            btns[i].style.backgroundColor = 'blue';
        } else {
            btns[i].style.backgroundColor = '';
        }
    }
})