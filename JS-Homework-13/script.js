
// let images  = ['img/1.jpg', 'img/2.jpg', 'img/3.JPG', 'img/4.png'],
//     length  = images.length,
//     index   = 1;
// setInterval(function() {
//     if(index == length) index = 0;
//     document.getElementById('image_id').src = images[index++];
//     }, 3000);

let tm = null;
let img = ['img/1.jpg', 'img/2.jpg', 'img/3.JPG', 'img/4.png'];
let pos = 0;

function slide_show() {
    if(pos  >= img.length)
        pos = 0;
    document.getElementById('image_id').src = img[pos];
    pos++;
}

function mode(id) {
    if(id == 1)
        tm = setInterval("slide_show()", 3000);
    else
        clearInterval(tm);
}

mode(1);

const imageWrapper =document.querySelector('.images-wrapper');

const startBtn = document.createElement('input');
startBtn.type = 'button';
startBtn.value = 'Припинити';
startBtn.addEventListener('click', () => mode(0));

const stopBtn = document.createElement('input');
stopBtn.type = 'button';
stopBtn.value = 'Відновити показ';
stopBtn.addEventListener('click', () => mode(1));

document.body.append(startBtn, stopBtn);