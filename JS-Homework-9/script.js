function arrayToLi(myArray, parent = document.body) {
    const ul = document.createElement('ul');

    for (let item in myArray) {
        let li = document.createElement('li');

        li.innerHTML = myArray[item];
        ul.appendChild(li);
    }

    parent.appendChild(ul);
}

const myArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

arrayToLi(myArray);