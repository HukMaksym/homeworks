import React from "react";
import { Outlet } from "react-router-dom";
import { Loader } from "../loader";
import styles from "../../styles/mainRight.module.scss";

export const MainRight = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInCart,
  handleCartClick,
  setModal,
  setModalData,
}) => {
  return (
    <div className={styles.MainRight}>
      {fetchError && <h2>Error: ${fetchError}</h2>}
      {isDataLoading ? (
        <Loader />
      ) : (
        <Outlet
          context={{
            products,
            favorites,
            handleFavoritesClick,
            productsInCart,
            handleCartClick,
            setModal,
            setModalData,
          }}
        />
      )}
    </div>
  );
};
