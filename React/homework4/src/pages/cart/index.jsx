import React from "react";
import styles from "../../styles/cart.module.scss";
import { Product } from "../../components/product";
import { useSelector } from "react-redux";

const getProducts = (state) => state.products;

export const Cart = () => {
  // get products from Cart
  const { items, productsInCart } = useSelector(getProducts);

  // set products to Cart
  const productsCart = items.filter((product) =>
    productsInCart.find((item) => item.id === product.id)
  );

  // check flag if Cart is empty
  const emptyCart = !productsCart.length;

  return (
    <div className={styles.Cart}>
      {emptyCart && <h1>Товари в кошику відсутні</h1>}
      {!emptyCart && (
        <ul className={styles.CartList}>
          {productsCart.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
