import React from "react";
import styles from "../../styles/main.module.scss";
import { MainLeft } from "../mainLeft";
import { MainRight } from "../mainRight";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <MainLeft />
      <MainRight />
    </div>
  );
};
