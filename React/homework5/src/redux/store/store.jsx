import { configureStore } from "@reduxjs/toolkit";
import products from "../reducers/products";
import modal from "../reducers/modal";
import formBuy from "../reducers/formBuy";

export const store = configureStore({
  reducer: {
    products,
    modal,
    formBuy,
  },
});
