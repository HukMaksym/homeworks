import React from "react";
import { useSelector } from "react-redux";
import { Product } from "../../components/product";
import { Loader } from "../../components/loader";
import styles from "../../styles/shop.module.scss";

const getProducts = (state) => state.products;

export const Shop = () => {
  // get status upload products
  const { error, status, items } = useSelector(getProducts);

  return (
    <div className={styles.Shop}>
      {status === "pending" && <Loader />}
      {error && <h2>{error}</h2>}

      {!error && status === "fulfilled" && (
        <ul className={styles.ShopList}>
          {items.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
