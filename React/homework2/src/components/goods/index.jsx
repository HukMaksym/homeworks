import React, { Component } from "react";
import styles from "../../styles/goods.module.scss";
import PropTypes from "prop-types";

import { Good } from "../good";

export class Goods extends Component {
  constructor(props) {
    super(props);
    this.bags = props.bags;
  }

  render() {
    return (
      <div className={styles.GoodsContainer}>
        <ul className={styles.Goods}>
          {this.bags.map((item) => (
            <Good
              key={item.id}
              item={item}
              addStorageValue={this.props.addStorageValue}
              removeStorageValue={this.props.removeStorageValue}
            ></Good>
          ))}
        </ul>
      </div>
    );
  }
}

Goods.propTypes = {
  bags: PropTypes.array.isRequired,

  addStorageValue: PropTypes.func.isRequired,
  removeStorageValue: PropTypes.func.isRequired,
};