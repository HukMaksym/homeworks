const form = document.querySelector(`.password-form`);

form.addEventListener('click', (evt) => {
    const targetI = evt.target.closest('i');
    const targetInput = evt.target.previousElementSibling;

    if (!targetI) return;

    if (targetI.classList.contains('fa-eye')) {
        targetI.classList.remove('fa-eye');
        targetI.classList.add('fa-eye-slash');
        targetInput.setAttribute('type', 'text');
    } else {
        targetI.classList.remove('fa-eye-slash');
        targetI.classList.add('fa-eye');
        targetInput.setAttribute('type', 'password');
    }
});


const pass1 = document.querySelector('#pass1');
const pass2 = document.querySelector('#pass2');
const btn = document.querySelector('.btn');

btn.addEventListener('click', (evt) => {
    evt.preventDefault();
    if (document.querySelector('span')) document.querySelector('span').remove();

    const alertMessage = document.createElement('span');
    alertMessage.innerText = "Потрібно ввести однакові значення";
    alertMessage.style.color = '#F00';

    if (pass1.value != pass2.value || pass2.value == '') {
        pass2.parentNode.append(alertMessage);
    } else {
        alert('You are welcome');
    }
});