let name = "";
let age;

while (name == "" || !(+age)) {
    age = ""; 		//обнуляем предыдущее неверное значение, если такое было
    name = prompt("Input Your Name", name);
    age = prompt("Input Your Age", age);
    if (age == null || name == null) {
        break; 		//при нажатии Cancel прерываем все
    }
}

switch (true) {
    case (age < 18):
        alert("You are not allowed to visit this website");
        break;
    case (age >= 18 && age <= 22):
        let confirmation = confirm("Are you sure you want to continue?");
        if (confirmation == true) {
            alert(`Welcome, ${name} !`);
        } else {
            alert("You are not allowed to visit this website");
        }
        break;
    case (age > 22):
        alert(`Welcome, ${name} !`);
        break;
    default:
        alert("Incorrect input data");
}