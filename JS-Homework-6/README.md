
## HW6 - user-age-and-password

### Відповіді на теоретичні питання:

#### 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Екранування - це додавання спецсимволів перед символами або строками коду для попередження їх некоректної інтерпретації. Наприклад додавання символу \ перед лапками інтерпретує самі лапки як частину тексту.

#### 2. Які засоби оголошення функцій ви знаєте?

- Function Declaration - наприклад:

   `function myFunc(param) {
     ..тіло функції..;
   }`

- Function Expression - наприклад:

   `let myVar = function(param) {
     ..тіло функції..;
   }`

#### 3. Що таке hoisting, як він працює для змінних та функцій?
Hoisting - це "підняття" оголошення змінних та функцій при коипіляції коду у верхню частину їх функціональної області видимості, або в глобальну область видимості. Це дозволяє використовувати функції та змінні ще до їх оголошення в коді.


### Завдання

Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:

- Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
    1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
    2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
    3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
- Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

