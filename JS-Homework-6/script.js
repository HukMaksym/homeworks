function createNewUser(firstName, lastName, birthday) {
    return {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        get getLogin() {
            return (this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase())
        },
        get getAge() {
            // return (new Date().getFullYear() - +this.birthday.slice(-4))
            const day = this.birthday.split('.')[0];
            const month = this.birthday.split('.')[1];
            const year = this.birthday.split('.')[2];
            return (Math.floor((new Date() - new Date(year, month - 1, day)) / (1000 * 60 * 60 * 24 * 365)));
        },
        get getPassword() {
            return (this.firstName.toUpperCase().slice(0, 1) + this.lastName.toLowerCase() + this.birthday.slice(-4))
        }
    };
}

let firstName = prompt("Input First Name:", "Ivan");
let lastName = prompt("Input Last Name:", "Kravchenko");
let birthday = prompt("Input day of birth (dd.mm.yyy):", "01.01.1970");

const newUser = createNewUser(firstName, lastName, birthday);

console.log(`First Name: ${newUser.firstName}`);
console.log(`Last Name: ${newUser.lastName}`);
console.log(`birthday: ${newUser.birthday}`);
console.log(`age: ${newUser.getAge} years old`);
console.log(`Login: ${newUser.getLogin}`);
console.log(`Password: ${newUser.getPassword}`);