// Я зробив два варіанти коду, не знаю як буде більш правильно. Обидва ніби то працюють..
//
// Варіант 1
//
function createNewUser(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        get getLogin() {
           return (this.firstName.toLowerCase().slice(0,1) + this.lastName.toLowerCase())
        }
    };
}
let firstName = prompt("Input First Name:", "Ivan");
let lastName = prompt("Input Last Name:", "Kravchenko");

const newUser = createNewUser(firstName, lastName);

console.log(newUser.getLogin);


// Варіант 2
//
//function createNewUser(firstName, lastName) {
//    this.firstName = firstName;
//    this.lastName = lastName;
//    this.getLogin = firstName.toLowerCase().slice(0,1) + lastName.toLowerCase();
//    console.log(`Login: ${this.getLogin}`);
//}
//
//const newUser = new createNewUser(prompt('Input First Name', 'Ivan'), prompt('Input Last Name', 'Kravchenko'));
