## Відповіді на теоретичні питання:

**1. Опишіть своїми словами, що таке метод об'єкту **

Метод об'єкту - це функцієя, що присвоєна властивості об'єкту. 


**2. Який тип даних може мати значення властивості об'єкта? **

Ті ж самі типи даних, що можуть мати звичайні змінні.


**3. Об'єкт це посилальний тип даних. Що означає це поняття? **

Є дві категорії типів даних - значенневий тип та посилальний тип. Значення значенневого типу - це фактичне значення, а значення посилального типу - це посилання на якесь значення. Тобто можна сказати що об'єкт лише вказує на дані, а не зберігає їх.
