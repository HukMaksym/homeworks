class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName() {
        return this.name;
    }
    getAge() {
        return this.age;
    }
    getSalary() {
        return this.salary;
    }
    setName(name) {
        this.name = name;
    }
    setAge(age) {
        this.age = age;
    }
    setSalary(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    getSalary() {
        return this.salary * 3;
    }
    getLang() {
        return this.lang;
    }
    setLang(lang) {
        return (this.lang = lang);
    }
}

const programmer = new Programmer('Andriy', 20, 800, 'ukrainian, english');
console.log(programmer);
console.log(`Name: ${programmer.getName()}`);
console.log(`Age: ${programmer.getAge()}`);
console.log(`Lang: ${programmer.getLang()}`);
console.log(`Salary: ${programmer.getSalary()}`);

const programmer2 = new Programmer('Serhiy', 21, 900, 'ukrainian, english');
console.log(programmer2);
console.log(`Age: ${programmer2.getAge()}`);
console.log(`Lang: ${programmer2.getLang()}`);
console.log(`Salary: ${programmer2.getSalary()}`);

const programmer3 = new Programmer('Petro', 22, 1000, 'polish, german');
console.log(programmer3);
console.log(`Age: ${programmer3.getAge()}`);
console.log(`Lang: ${programmer3.getLang()}`);
console.log(`Salary: ${programmer3.getSalary()}`);
