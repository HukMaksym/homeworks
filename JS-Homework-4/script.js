let firstNumber, secondNumber, operator;

while (!(+firstNumber) || !(+secondNumber)) {
    firstNumber = prompt("Input first number", firstNumber);
    secondNumber = prompt("Input second number", secondNumber);
    operator = prompt("Input operator '+', '-', '*', '/' ", operator);
}

let result = calcNumbers(firstNumber, secondNumber, operator);
console.log(result);

function calcNumbers(firstNum, secondNum, operator) {
    switch (operator) {
        case "+":
            return +firstNum + +secondNum;
        case "-":
            return firstNum - secondNum;
        case "*":
            return firstNum * secondNum;
        case "/":
            return firstNum / secondNum;
        default:
            alert("Incorrect input data");
    }
}