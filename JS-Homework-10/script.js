const tabs = () => {
    const head = document.querySelector('.tabs')
    const body = document.querySelector('.tabs-content')

    const getActiveTabName = () => {
        return head.querySelector('.active').dataset.tab
    }

    const setActiveContent = () => {
        if (body.querySelector('.active')) {
            body.querySelector('.active').classList.remove('active')
        }
        body.querySelector(`[data-tab=${getActiveTabName()}]`).classList.add('active')
    }
    setActiveContent(getActiveTabName())

    head.addEventListener('click', e => {
        const caption = e.target.closest('.tabs-title')
        if (!caption) return
        if (caption.classList.contains('active')) return
        if (head.querySelector('.active')) {
            head.querySelector('.active').classList.remove('active')
        }
        caption.classList.add('active')
        setActiveContent(getActiveTabName())
    })
}

tabs()