const paragraphs = document.querySelectorAll('.main-text-paragraph');
const asideLinks = document.querySelectorAll('.aside-link');
const footerLinks = document.querySelectorAll('.footer-link');
const copyright = document.querySelector('.footer-copyright');
let flag;

function themeNight() {
    document.body.style.backgroundColor = "#484848";
    paragraphs.forEach(link => {
        link.style.color = "#bfbfbf";
    })
    asideLinks.forEach(link => {
        link.style.color = "#4BCAFF";
    })
    footerLinks.forEach(link => {
        link.style.color = "#bfbfbf";
    })
    copyright.style.color = "#bfbfbf";
}

function themeDay() {
    document.body.style.backgroundColor = "";
    paragraphs.forEach(link => {
        link.style.color = "";
    })
    asideLinks.forEach(link => {
        link.style.color = ""
    })
    footerLinks.forEach(link => {
        link.style.color = "";
    })
    copyright.style.color = "";
}

if (localStorage.getItem('theme') === 'night') {
    flag = 'night';
    themeNight();
}

const changeThemeBtn = document.querySelector('.change-theme-link');
changeThemeBtn.addEventListener('click', () => {
    if (flag === 'day' || flag === undefined) {
        themeNight();
        flag = 'night';
    } else {
        themeDay();
        flag = 'day';
    }
    localStorage.setItem('theme', flag);
})
