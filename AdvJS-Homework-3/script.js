// 1 Task - combining two arrays --------------------------------------------
const clients1 = [
    "Гилберт",
    "Сальваторе",
    "Пирс",
    "Соммерс",
    "Форбс",
    "Донован",
    "Беннет"
];
const clients2 = [
    "Пирс",
    "Зальцман",
    "Сальваторе",
    "Майклсон"
];

console.log('----- 1 Task - combining two arrays:');
const allClients = [...new Set([...clients1, ...clients2])];
console.log(allClients);

// 2 Task - array "charactersShortInfo" ---------------------------------------
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

console.log('----- 2 Task - array "charactersShortInfo":');
function getShortInfo(arr) {
    const result = [];
    arr.forEach(({ name, lastName, age }) => {
        result.push({ name, lastName, age });
    });
    return result;
}

const charactersShortInfo = getShortInfo(characters);
console.log(charactersShortInfo);

// 3 Task - variables: name, years, isAdmin ---------------------------------------
const user1 = {
    name: "John",
    years: 30
};

console.log('----- 3 Task - variables: name, years, isAdmin:');

const { name, years: age, isAdmin = false } = user1;
console.log(`Name: ${name}`);
console.log(`Age: ${age}`);
console.log(`Is Admin: ${isAdmin}`);

// ----- 4 Task - brief Satoshi Nakamoto ---------------------------------------
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

console.log('----- 4 Task - brief Satoshi Nakamoto:');

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
console.log(fullProfile);

// ----- 5 Task - add book to array ---------------------------------------
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

console.log('----- 5 Task - add book to array:');
const books2 = [...books, {...bookToAdd}];
console.log(books2);

// ----- 6 Task - new object "employee" ---------------------------------------
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

console.log('----- 6 Task - new object "employee":');

const employee2 = { ...employee, age: 51, salary: 5500 };
console.log(employee2);

// ----- 7 Task - complete the code ---------------------------------------
const array = ['value', () => 'showValue'];

// Допишіть код тут
console.log('----- 7 Task - complete the code:');

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'

console.log('----- Results at alerts');