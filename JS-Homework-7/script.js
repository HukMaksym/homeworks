const testArray = ['hello', 'world', 35, '23', null, true, {}];
const allTypes = ['string', 'number', 'boolean', 'object'];

function filterBy(arr, type) {
    return arr.filter((item) => {
        return (typeof item !== type)
    });
}

allTypes.forEach(type => console.log(`Array without ${type}:`) + console.log(filterBy(testArray, type)));