const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');
const isValid = ['author', 'name', 'price'];

function validation(item) {
    for (const value of isValid) {
        try {
            if (!item[value]) {
                throw new Error(`${value} not found`);
            }
        } catch (error) {
            console.log(item);
            console.log(error.message);
            return false;
        }
    }
    return true;
}

function setBooks(items) {
    const booksContainer = document.createElement('ul');
    booksContainer.classList.add('books');
    root.append(booksContainer);

    items.forEach((item) => {
        const itemContainer = document.createElement('li');
        itemContainer.classList.add('books__item');

        for (const value of isValid) {
            const bookValue = document.createElement('div');
            bookValue.classList.add(value);
            bookValue.innerText = `${value} : ${item[value]}`;

            itemContainer.append(bookValue);
            booksContainer.append(itemContainer);
        }
    });
}

function getBooks(books) {
    const result = [];
    books.forEach((item) => {
        if (validation(item)) {
            result.push(item);
        }
    });
    setBooks(result);
}

getBooks(books);
